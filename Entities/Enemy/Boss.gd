extends KinematicBody2D

var direction = -1
var dead = false
var vSpeed = 0
var hSpeed = 800

var health = 3

var velocity = Vector2.ZERO
var UP = Vector2(0,-1)

onready var ani = $AnimatedSprite

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func _physics_process(_delta):
	velocity.y = hSpeed
	if (direction == -1 and !dead and !is_on_wall()):
		ani.flip_h = true
		ani.play("run")
		velocity.x = -400
	elif (direction == 1 and !dead and !is_on_wall()):
		ani.flip_h = false
		ani.play("run")
		velocity.x = 400
	else:
		if (!dead and is_on_wall()):
			if (ani.flip_h):
				ani.flip_h = false
				velocity.x = 400
				direction = 1
			elif (!ani.flip_h):
				ani.flip_h = true
				velocity.x = -400
				direction = -1
		else:
			ani.play("dead")
# warning-ignore:return_value_discarded
	move_and_slide(velocity,UP)

func _on_Robot_area_entered(area):
	if (area.is_in_group("slash")):
		health-=1
		if health == 0:
			dead = true
			velocity.x = 0
			ani.play("dead")
	pass # Replace with function body.

func _on_AnimatedSprite_animation_finished():
	if (ani.animation == "dead"):
		queue_free()
	pass # Replace with function body.
