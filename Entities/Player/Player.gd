extends KinematicBody2D

export var gravity = 2000
export var acceleration = 2000
export var deacceleration = 2000
export var friction = 2000
export var current_friction = 2000
export var max_horizontal_speed = 550
export var max_fall_speed = 700
export var jump_height = -1300
export var double_jump_height = -1100
export var slide_friction = 200
export var glide_fall_speed = 200
export var glide_speed = 300

var last_position = self.position

var vSpeed = 0
var hSpeed = 0

var touching_ground : bool = false
var touching_wall : bool = false
var is_jumping : bool = false
var is_double_jumping : bool = false
var is_sliding : bool = false
var is_slashing : bool = false
var can_slide : bool = false
var air_jump_pressed : bool = false
var coyote_time : bool = false
var can_double_jump : bool = false
var can_slash : bool = false
var dead : bool = false

var motion = Vector2.ZERO
var UP = Vector2(0,-1)

onready var ani = $AnimatedSprite
onready var stand_shape = $StandShape
onready var slide_shape = $PlayerCollision/SlideShape
onready var slash_shape_right = $SlashCollision/SlashShapeR
onready var slash_shape_left = $SlashCollision/SlashShapeL
onready var slash_area = $PlayerCollision

###
func _ready():
	pass # Replace with function body.

func _physics_process(var delta):
	check_ground_logic()
	handle_player_collision_shapes()
	handle_input(delta)
	do_physics(delta)

func check_ground_logic():
	if is_on_floor():
		max_horizontal_speed = 550
		max_fall_speed = 700
		touching_ground = true
		coyote_time = true
		yield(get_tree().create_timer(0.2),"timeout") # we pause here for 200 milliseoncds
		coyote_time = false
	else:
		touching_ground = false
		slash_shape_right.disabled = true
		slash_shape_left.disabled = true

func handle_player_collision_shapes():
	#sliding
	if(is_sliding and slide_shape.disabled):
		stand_shape.disabled = true
		slide_shape.disabled = false
	#standing
	elif(!is_sliding and stand_shape.disabled):
		stand_shape.disabled = false
		slide_shape.disabled = true

func handle_input(var delta):
	check_sliding_logic()
	handle_movement(delta)
	handle_jumping(delta)
	handle_slashing(delta)

func do_physics(var delta):
	if(is_on_ceiling()):
		motion.y = 10
		vSpeed = 10
		
	vSpeed += (gravity * delta) # apply gravity downwards
	
	vSpeed = min(vSpeed,max_fall_speed) # limit how fast we can fall
	
	#update our motion vector
	motion.y = vSpeed
	motion.x = hSpeed
	
	#apply our motion vectgor to move and slide
	motion = move_and_slide(motion,UP)

func check_sliding_logic():
	#check if it's possible to slide
	if(abs(hSpeed) > (max_horizontal_speed -1) and touching_ground):
		if(!is_sliding):
			can_slide = true
	else:
		can_slide = false
	#check if we're holding down the slide key/button
	#if(can_slide and Input.is_action_pressed("ui_down")):
	#	is_sliding = true
	#	can_slide = false
	#check if we're sliding but just released the slide key
	if(is_sliding and !Input.is_action_pressed("ui_down")):
		is_sliding = false
	#do animation and friction logic 
	if(is_sliding and touching_ground):
		current_friction = slide_friction # reduce our friction for our slide
		ani.play("slide")
	else:
		current_friction = friction
		is_sliding = false
		
func handle_movement(var delta):
	if(is_on_wall()):
		hSpeed = 0
		motion.x = 0
	#controller right/keyboard right
	if((Input.is_action_pressed("ui_right")) and (!is_sliding or !is_slashing)):
		slash_shape_right.disabled = true
		slash_shape_left.disabled = true
		if(hSpeed <-100):
			hSpeed += (deacceleration * delta)
			if(touching_ground):
				ani.play("turn")
		elif(hSpeed < max_horizontal_speed):
			hSpeed += (acceleration * delta)
			ani.flip_h = false
			if(touching_ground):
				ani.play("run")
		else:
			if(touching_ground):
				if (Input.is_action_just_pressed("ui_accept")):
					handle_slashing(delta)
					can_slash = true
				ani.play("run")
	elif((Input.is_action_pressed("ui_left")) and (!is_sliding or !is_slashing)):
		slash_shape_right.disabled = true
		slash_shape_left.disabled = true
		if(hSpeed > 100):
			hSpeed -= (deacceleration * delta)
			if(touching_ground):
				ani.play("turn")
		elif(hSpeed > -max_horizontal_speed):
			hSpeed -= (acceleration * delta)
			ani.flip_h = true
			if(touching_ground):
				ani.play("run")
		else:
			if(touching_ground):
				ani.play("run")
	elif (Input.is_action_just_pressed("ui_accept") and touching_ground and !is_sliding):
		handle_slashing(delta)
		can_slash = true
	#elif (Input.is_action_just_pressed("ui_accept") and !touching_ground and !is_sliding):
	#	handle_jumping_slash(delta)
	#	can_slash = true
	else:
		if(touching_ground):
			if (ani.animation == "slash"):
				pass
			elif (!is_sliding or !is_slashing):
				slash_shape_right.disabled = true
				slash_shape_left.disabled = true
				ani.play("idle")
			else:
				if(abs(hSpeed) < 0.2):
					ani.stop()
					ani.frame = 1
		hSpeed -= min(abs(hSpeed),current_friction * delta) * sign(hSpeed)
	pass

func handle_jumping(_delta):
	if (coyote_time and Input.is_action_just_pressed("ui_select")):
		vSpeed = jump_height
		is_jumping = true
		can_double_jump = true
	
	if (touching_ground):
		max_horizontal_speed = 500
		max_fall_speed = 700
		if ((Input.is_action_just_pressed("ui_select") or air_jump_pressed) and !is_jumping):
			vSpeed = jump_height
			is_jumping = true
			touching_ground = false
	else:
		#Do variable jump logic
		if (vSpeed < 0 and !Input.is_action_pressed("ui_select") and !is_double_jumping):
			vSpeed = max(vSpeed,jump_height / 2)
		if (can_double_jump and Input.is_action_just_pressed("ui_select") and !coyote_time):
			vSpeed = double_jump_height
			ani.play("doublejump")
			is_double_jumping = true
			can_double_jump = false
		#Do some animation logic on the jump
		if (!is_double_jumping and vSpeed <0):
			ani.play("jump")
		elif (!is_double_jumping and vSpeed > 0):
			if (Input.is_action_pressed("ui_select")):
				max_fall_speed = glide_fall_speed
				max_horizontal_speed = glide_speed
				ani.play("glide")
			else:
				max_horizontal_speed = 550
				max_fall_speed = 700
				ani.play("fall")
		elif (is_double_jumping and ani.frame == 3):
			is_double_jumping = false
		#check if we're pressing jump just before we land on a platform
		if (Input.is_action_just_pressed("ui_select")):
			air_jump_pressed = true
			yield(get_tree().create_timer(0.2),"timeout")
			air_jump_pressed = false

func handle_slashing(_delta):
	if (can_slash):
		ani.play("slash")
		is_slashing = true
		can_slash = false
		if (!ani.flip_h):
			slash_shape_right.disabled = false
		elif (ani.flip_h):
			slash_shape_left.disabled = false
	pass
	
func handle_jumping_slash(_delta):
	if (can_slash):
		ani.play("slashjump")
		is_slashing = true
		can_slash = false
		if (!ani.flip_h):
			slash_shape_right.disabled = false
		elif (ani.flip_h):
			slash_shape_left.disabled = false
	pass
	
func hit_checkpoint(checkpoint_pos):
	last_position = checkpoint_pos
	print(last_position)
	
func hit_something():
	global.lives-=1
	if global.lives == 0:
		get_tree().change_scene("res://Scenes/GameOver.tscn")
	else: 
		self.position = last_position

func _on_AnimatedSprite_animation_finished():
	if (ani.animation == "slash"):
		ani.play("idle")
		slash_shape_right.disabled = true
		slash_shape_left.disabled = true
		is_slashing = false
	elif (ani.animation == "dead"):
		queue_free()

func _on_PlayerCollision_area_entered(area):
	if (area.is_in_group("robotbody")):
		hit_something()
	pass # Replace with function body.
