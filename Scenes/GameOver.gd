extends ColorRect
onready var timer = get_node("Timer")

var seconds = 5

# Called when the node enters the scene tree for the first time.
func _ready():
	timer.set_wait_time(1.1)
	timer.start()

func _on_Timer_timeout():
	seconds-=1
	get_node("Time").set_text(str(seconds))
	if seconds == -1:
		get_tree().change_scene("res://Scenes/Level 1.tscn")
