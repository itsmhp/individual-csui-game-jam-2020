extends Area2D

export (String) var RespawnInScene = "Insert Scene Here"

func _on_Finish_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene("res://Scenes/Finish.tscn")
